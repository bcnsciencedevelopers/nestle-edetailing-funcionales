/* Author: Ruth CJ */

jQuery(document).ready(function() {

//    var navBoletes = new Bullets(estructuraMenu[1].children, 'zona-boletes', 2);
//    navBoletes.init(1);

    
    var indexActius = { "nivell_1": "2", "nivell_2": false, "nivell_3": false};
    menuPrincipal.init(indexActius);
    
    // Deshabilitar un element del menú en aquest slide
    // 1r paràmetre: index de l'element a deshabilitar (comença des de 0)
    // 2n apràmetre: nivell del menú (comença des de 1)
//    menuPrincipal.disableMenuItem(3,1);

    //Inicialitzar l'edetailing 
    edetailing.init(estructura);
    
    edetailing.indexSlideActual = "3";

    //Indicar que se debe usar swipe con la libreria Hammer.
    // edetailing.hammerSwipe(document.body);
    
});

