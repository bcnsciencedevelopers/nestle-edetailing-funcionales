var menuPrincipal;
var estructuraMenu;
var estructura;

$(document).ready(function () {
//    var slide = 0;
//    var presentation = 0;
    /**
     * Array que representa l'estructura del material.
     * id de les CLM Presentation
     * + el nom de l'arxiu ZIP de cada Key Message (sense el .zip) de cada presentació
     * */

    // #guia - escriu aquí l'estructura de la teva presentació
    estructura = [];

    estructura[0] = ["ES_2017_IF_edetailing_funcionales_00", [
            "ES_2017_IF_edetailing_funcionales_00_00",
            "ES_2017_IF_edetailing_funcionales_00_01"]];

    estructura[1] = ["ES_2017_IF_edetailing_funcionales_01", [
            "ES_2017_IF_edetailing_funcionales_01_00",
            "ES_2017_IF_edetailing_funcionales_01_01",
            "ES_2017_IF_edetailing_funcionales_01_02",
            "ES_2017_IF_edetailing_funcionales_01_03",
            "ES_2017_IF_edetailing_funcionales_01_04",
            "ES_2017_IF_edetailing_funcionales_01_05",
            "ES_2017_IF_edetailing_funcionales_01_06"
        ]];

    estructura[2] = ["ES_2017_IF_edetailing_funcionales_02", [
            "ES_2017_IF_edetailing_funcionales_02_00",
            "ES_2017_IF_edetailing_funcionales_02_01",
            "ES_2017_IF_edetailing_funcionales_02_02",
            "ES_2017_IF_edetailing_funcionales_02_03",
            "ES_2017_IF_edetailing_funcionales_02_04",
            "ES_2017_IF_edetailing_funcionales_02_05"
        ]];

    
    estructuraMenu = [
        {
            "text": "<span class='textos'>PROBIÓTICOS </br>EN FÓRMULAS FUNCIONALES</span>",
//            "classes": "miau tokoto tracata",
            "ic": "icon-ic-adn",
            "goto": {
                "slide": "1",
                "presentation": "0",
                "touch": true
            },
            "children": false
        },
        {
            "text": "<span class='textos'>LA MICROBIOTA </br>Y EL SISTEMA INMUNITARIO</span>",
            "ic": "icon-ic-escudo",
           "goto": {
               "slide": "0",
               "presentation": "1"
            },
            "children": false
        },
        {
            "text": "<span class='textos'>LOS EFECTOS </br>DE <i>L.REUTERI</i></span>",
            "ic": "icon-ic-bacterias",
            "goto": {
                "slide": "1",
                "presentation": "1"
            },
            "children": false
        },
        {
            "text": "<span class='textos'>TODA UNA GAMA DE SOLUCIONES</span>",
            "ic": "icon-ic-solu",
            "goto": {
                "slide": "5",
                "presentation": "1"
            },
            "children": false
        },
                {
            "text": "<span class='textos'>NOTA IMPORTANTE</span>",
            "ic": "icon-ic-lapiz",
            "goto": {
                "slide": "6",
                "presentation": "1"
            },
            "children": false
        },
        
    ];
    // 2n paràmetre: id de l'element on s'inclourà el menú
    menuPrincipal= new Menu(estructuraMenu, 'mainmenu');

    // #guia - IMPORTANT! Comentar aquesta línia al pujar al Salesforce
    // Habilitar el mode Test durant el desenvolupament i per testejar i debugar funcions de veeva! 
   //edetailing.enableTestMode();
    
    edetailing.debugBox = document.getElementById('returned-results');

    // Opcional: modificar valors per defecte. Cal fer-ho abans d'inicialitzar l'edetailing.
    edetailing.changeOverlayDefault('opacity', 0.3);
    edetailing.changePopupDefault('temps', 300);
//    edetailing.changeTouchEventDefault('touchstart');

    // 2n paràmetre: id de l'element on s'inclourà el codi

});



