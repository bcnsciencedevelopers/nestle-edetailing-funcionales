/* Author: Ruth CJ */

jQuery(document).ready(function() {
    
    
    var indexActius = { "nivell_1": "3", "nivell_2": false, "nivell_3": false};
    menuPrincipal.init(indexActius);
    
    // Deshabilitar un element del menú en aquest slide
    // 1r paràmetre: index de l'element a deshabilitar (comença des de 0)
    // 2n apràmetre: nivell del menú (comença des de 1)
//    menuPrincipal.disableMenuItem(3,1);

    //Inicialitzar l'edetailing 
    edetailing.init(estructura);
    

    //Indicar que se debe usar swipe con la libreria Hammer.
    // edetailing.hammerSwipe(document.body);
    
});

