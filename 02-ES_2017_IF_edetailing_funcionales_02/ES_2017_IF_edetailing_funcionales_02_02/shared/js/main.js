/* Author: Ruth CJ */
var auxPregunta;
jQuery(document).ready(function () {
    var cont = 0;
    var cont2 = 0;

    $('.activador').on('click', function () {
        if (cont === 0) {
            $('#img').attr('src', 'img/popup_referencies2.png');
            $("#popup1-bola2").attr("class", "active bola abs bola-popup ");
            $("#popup1-bola1").attr("class", " bola abs bola-popup");
            $('#btn-next-popup').removeClass("active");
            $("#btn-prev-popup").addClass("active");
            $('#ocult').removeClass("left");
            $('#ocult').addClass("right");
//        $('#btn-next-popup').attr("class", " ");

            cont++;
        } else {
            $('#img').attr('src', 'img/popup_referencies.png');
            $("#popup1-bola1").attr("class", "active bola abs bola-popup");
            $("#popup1-bola2").attr("class", " bola abs bola-popup");
            $("#btn-next-popup").addClass("active");
            $('#btn-prev-popup').removeClass("active");
            $('#ocult').addClass("left");
            $('#ocult').removeClass("right");

            cont--;
        }
    });


    $('.activadorpop2').on('click', function () {
        if (cont2 === 0) {
            $('#img2').attr('src', 'img/popup_referencies4.png');
            $("#popup2-bola2").attr("class", "active bola abs bola-popup ");
            $("#popup2-bola1").attr("class", " bola abs bola-popup");
            $('#btn-next-popup2').removeClass("active");
            $("#btn-prev-popup2").addClass("active");
            $('#ocult2').removeClass("left");
            $('#ocult2').addClass("right");
//        $('#btn-next-popup').attr("class", " ");

            cont2++;
        } else {
            $('#img2').attr('src', 'img/popup_referencies3.png');
            $("#popup2-bola1").attr("class", "active bola abs bola-popup");
            $("#popup2-bola2").attr("class", " bola abs bola-popup");
            $("#btn-next-popup2").addClass("active");
            $('#btn-prev-popup2').removeClass("active");
            $('#ocult2').addClass("left");
            $('#ocult2').removeClass("right");

            cont2--;
        }
    });

    $("#popup_referencies").append("<div class='mano mano-cerrar referencia abs'></div>");
    $("#popup_referencies2").append("<div class='mano mano-cerrar referencia abs'></div>");
    $("#popup").append("<div class='mano mano-cerrar2 pop abs'></div>");


});




  
function saveClickStreamData(clickStream_data){
    com.veeva.clm.createRecord("Call_Clickstream_vod__c", clickStream_data, savedClickstream);      
}

function savedClickstream(result) {
    if (result.success){
        sessionStorage.setItem(auxPregunta+'desat', true);
    }else{
        sessionStorage.setItem(auxPregunta+'desat', false);
    }
}
