/*
 * Edetailings Library v 1.1 for Veeva CLM presentations - BCNscience 
 * Requirements: jQuery + veeva-library-4.0 or higher
 * Optional: Hammer.js
 * Author: Ruth CJ 
 * */

var edetailing;

edetailing = {
    testMode: false,
    debugBox: false,
    irepPresentation: true,
    defaultAccountName: "Maria",
    //Variables events touch
    defaultTouchEvent: 'touchend',
    // Variables navegació
    estructura: [],
    indexPresentacioActual: '',
    indexSlideActual: '',
    indexSlideAnterior: '',
    indexPresentacioAnterior: '',
    cami: '',
    // Variables per defecte als popups i popup actual

    popupDefault: {
        animacio: "slide-bottom",
        pixels: 1600,
        temps: 400
    },
    overlayDefault: {
        opacity: 0.2
    },
    popupActual: {
        idPopup: '',
        dataAnimacio: '',
        dataPixels: '',
        dataTemps: ''
    },
    saveClickstreamPopup: true,
    btnObridorPopup: '',
    classeBtnObridorPopupActiu: 'active',
    /**
     * Habilita el mode test per a treballar en local
     * o per fer proves al sandbox i posa el testMode de
     * la llibreria de Veeva a true tb
     */
    enableTestMode: function () {
        edetailing.testMode = true;
//        com.veeva.clm.testMode = true;
        console.info('Test mode enabled');
    },
    /**
     * Funció per a debbugar funcions de la llibreria de Veeva a l'entorn de test
     * (no tenim consola, de manera que podem treure els resultats per aquí)
     * @param {type} info: 
     */
    showResults: function showResults(info) {
        if (edetailing.testMode) {
            edetailing.debugBox.innerHTML += info;
        }
    },
    /**
     * Inicialitzar els 
     * @param {type} estructuraEdetailing
     */
    init: function (estructuraEdetailing) {
        console.info('init - edetailing');

        edetailing.estructura = estructuraEdetailing;
        edetailing.estructura = estructuraEdetailing;
        edetailing.initNav();
        edetailing.initPopups();
        edetailing.initReactiveButtons();

        // scroll prevention
        if (!edetailing.testMode) {
            document.ontouchmove = function (e) {
                e.preventDefault();
            };

            enabled_scrolls = document.getElementsByClassName("js-enable-scroll");
            for (i = 0; i < enabled_scrolls.length; i++) {
                enabled_scrolls[i].addEventListener("touchmove", enableScroll, false);
            }

            function enableScroll(e) {
                e.stopPropagation();
            }
        }

        edetailing.irepPresentation = edetailing.isIrepPresentation();

    },
    initNav: function () {
        console.info('init - Navigation functionality');
        edetailing.indexSlideActual = edetailing.getFromSessionStorage('slideActual', 0);
        edetailing.indexPresentacioActual = edetailing.getFromSessionStorage('presActual', 0);
        edetailing.indexSlideAnterior = edetailing.getFromSessionStorage('slideAnterior', 0);
        edetailing.indexPresentacioAnterior = edetailing.getFromSessionStorage('presAnterior', 0);
        edetailing.cami = edetailing.getFromSessionStorage('cami', 0);


        var btnsTouch = document.getElementsByClassName('js-goto-touch');
        for (var i = 0; i < btnsTouch.length; i++) {
            btnsTouch[i].addEventListener(edetailing.defaultTouchEvent, edetailing.goto);
        }

        var btnsNextTouch = document.getElementsByClassName('js-goto-next-touch');
        for (i = 0; i < btnsNextTouch.length; i++) {
            btnsNextTouch[i].addEventListener(edetailing.defaultTouchEvent, edetailing.gotoNext);
        }

        var btnsPrevTouch = document.getElementsByClassName('js-goto-prev-touch');
        for (i = 0; i < btnsPrevTouch.length; i++) {
            btnsPrevTouch[i].addEventListener(edetailing.defaultTouchEvent, edetailing.gotoPrev);
        }

        var btnsClick = document.getElementsByClassName('js-goto');
        for (i = 0; i < btnsClick.length; i++) {
            btnsClick[i].addEventListener("click", edetailing.goto);
        }

        var btnsNextClick = document.getElementsByClassName('js-goto-next');
        for (i = 0; i < btnsNextClick.length; i++) {
            btnsNextClick[i].addEventListener("click", edetailing.gotoNext);
        }

        var btnsPrevClick = document.getElementsByClassName('js-goto-prev');
        for (i = 0; i < btnsPrevClick.length; i++) {
            btnsPrevClick[i].addEventListener("click", edetailing.gotoPrev);
        }

        var btnBackClick = document.getElementsByClassName('js-goto-back');
        for (i = 0; i < btnBackClick.length; i++) {
            btnBackClick[i].addEventListener("click", edetailing.gotoBack);
        }
    },
    initPopups: function () {
        console.info('init - Popups functionality');
        var btnMostrarPopups = document.getElementsByClassName('js-mostrar-popup');
        for (var i = 0; i < btnMostrarPopups.length; i++) {
            btnMostrarPopups[i].addEventListener(edetailing.defaultTouchEvent, edetailing.openPopup);
        }
        var btnTancarPopups = document.getElementsByClassName('js-tancar-popup');
        for (i = 0; i < btnTancarPopups.length; i++) {
            btnTancarPopups[i].addEventListener("click", edetailing.closePopup);
        }
    },
    initReactiveButtons: function () {
        console.info('init - Reactive buttons functionality');
        var btnReactive = document.getElementsByClassName('btn');
        for (var i = 0; i < btnReactive.length; i++) {
            btnReactive[i].addEventListener("touchstart", edetailing.startReact);
            btnReactive[i].addEventListener("touchend", edetailing.endReact);
        }
    },
    /**
     * 
     * @param {type} codi
     * @param {type} id
     * @returns {undefined}
     */
    initCodi: function (codi, id) {
               var codiContainer = $('#' + id);
               codiContainer.html(codi);
    },
    /**
     * 
     * @param {type} varName
     * @param {type} defaultValue
     * @returns {edetailing.getFromSessionStorage.result|DOMString}
     */
    getFromSessionStorage: function (varName, defaultValue) {
        var result = sessionStorage.getItem(varName);
        if (result === null || typeof result === 'undefined' || result === '') {
            result = defaultValue;
            sessionStorage.setItem(varName, defaultValue);
        }
        return result;
    },
    /**
     * 
     * @param {type} e
     */
    goto: function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).hasClass('disabled'))
            return;
        edetailing.crearEnllac(this, 'goto');
    },
    /**
     * 
     * @param {type} e
     */
    gotoBack: function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).hasClass('disabled'))
            return;
        edetailing.crearEnllac(this, 'back');
    },
    /**
     * 
     * @param {type} e
     */
    gotoNext: function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).hasClass('disabled'))
            return;
        // Guardar slide anteriors i actual
        sessionStorage.setItem("slideAnterior", edetailing.indexSlideActual);
        sessionStorage.setItem("slideActual", edetailing.indexSlideActual + 1);

        com.veeva.clm.nextSlide();
    },
    /**
     * 
     * @param {type} e
     */
    gotoPrev: function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).hasClass('disabled'))
            return;

        // Guardar slide anteriors i actual
        sessionStorage.setItem("slideAnterior", edetailing.indexSlideActual);
        sessionStorage.setItem("slideActual", edetailing.indexSlideActual - 1);

        com.veeva.clm.prevSlide();
    },
    /**
     * 
     * @param {type} aixo
     * @param {type} tipoNavegacion
     * @param {type} direccion
     */
    crearEnllac: function (aixo, tipoNavegacion, direccion) {
        var auxSlide = "";
        var auxPresentation = "";
        var indexSlide, indexPresentacio;
        var indexSlideCami, indexPresentacioCami;
        var direccion = direccion || '';
        switch (tipoNavegacion) {
            case 'goto':
                // Agafem les dades dels data-attr de l'element clicat
                indexSlide = aixo.dataset[direccion + 'slide'];
                indexSlideCami = aixo.dataset[direccion + 'slide-' + edetailing.cami];

                if (typeof indexSlideCami !== 'undefined' && indexSlideCami !== false) {
                    indexSlide = indexSlideCami;
                }
                indexPresentacio = aixo.dataset[direccion + 'presentation'];
                indexPresentacioCami = aixo.dataset[direccion + 'presentation-' + edetailing.cami];

                // Mirem si existeix data-presentació-'cami'
                if (typeof indexPresentacioCami !== 'undefined' && indexPresentacioCami !== false) {
                    indexPresentacio = indexPresentacioCami;
                    auxPresentation = edetailing.estructura[indexPresentacio][0];
                    //Sinó, mirem si existeix un data-presentacio
                } else if (typeof indexPresentacio !== 'undefined' && indexPresentacio !== false) {
                    // Si: guardem a auxPresentation la id de la CLM Presentation corresponent segons l'estructura del material
                    auxPresentation = edetailing.estructura[indexPresentacio][0];
                } else {
                    // No: auxPresentation ha de seguir sent "" ja que és un enllaç intern. Igualment, cal posar el index de la presentació actual a indexPresentacio per tal que funcioni en mode DEBUG
                    indexPresentacio = edetailing.indexPresentacioActual;
                }
                break;
            case 'back':
                // Agafem les dades del session storage
                indexSlide = edetailing.indexSlideAnterior;
                indexPresentacio = edetailing.indexPresentacioAnterior;
                auxPresentation = edetailing.estructura[indexPresentacio][0];
                break;
        }

        // Guardem a auxSlide el nom de l'arxiu del Key Message corresponent segons l'estructura del material
        auxSlide = edetailing.estructura[indexPresentacio][1][indexSlide];

        // Guardar slide i presentació anteriors
        sessionStorage.setItem("slideAnterior", edetailing.indexSlideActual);
        sessionStorage.setItem("presAnterior", edetailing.indexPresentacioActual);

        // Actualitzem l'índex de la presentació actual
        edetailing.indexPresentacioActual = indexPresentacio;
        sessionStorage.setItem("presActual", edetailing.indexPresentacioActual);

        // Actualitzem l'índex del slide actual
        edetailing.indexSlideActual = indexSlide;
        sessionStorage.setItem("slideActual", edetailing.indexSlideActual);

        // Modifiquem el camí si així s'indica
         if (aixo.dataset.choosepath) {
            var cami = aixo.dataset['cami-' + edetailing.cami];
            if (typeof cami !== 'undefined' && cami !== false) {
                edetailing.cami = cami;
            } else {
                cami = aixo.dataset.cami;
                if (typeof cami !== 'undefined' && cami !== false) {
                    edetailing.cami = cami;
                }
            }
            sessionStorage.setItem("cami", edetailing.cami);
        }

        // Creem l'enllaç en mode DEBUG o PRODUCCIÓ segons toqui
        if (edetailing.testMode) {
            auxPresentation = edetailing.estructura[indexPresentacio][0];
            if (indexPresentacio < 10)
                indexPresentacio = "0" + indexPresentacio;
            location.href = "../../" + indexPresentacio + "-" + auxPresentation + "/" + auxSlide + "/" + auxSlide + ".html";
        } else {
            com.veeva.clm.gotoSlide(auxSlide + ".zip", auxPresentation);
        }
    },
    /**
     * 
     * @param {type} e
     * @param {type} aixodesdefora
     */
    openPopup: function (e, aixodesdefora) {
        e.stopPropagation();
        e.preventDefault();
        var aixo = aixodesdefora || $(this);
        // Si l'element està desactivat, no fer res!
        if (aixo.hasClass('disabled'))
            return;

        edetailing.btnObridorPopup = aixo;
        aixo.addClass(edetailing.classeBtnObridorPopupActiu);
        edetailing.popupActual.idPopup = aixo.data('popup');
        var popup = $(edetailing.popupActual.idPopup);
        var overlay = $("#overlay");

        edetailing.popupActual.dataAnimacio = popup.attr('data-animacio');
        if (typeof edetailing.popupActual.dataAnimacio === 'undefined' || edetailing.popupActual.dataAnimacio === false) {
            edetailing.popupActual.dataAnimacio = edetailing.popupDefault.animacio;
        }
        edetailing.popupActual.dataPixels = popup.attr('data-pixels');
        if (typeof edetailing.popupActual.dataPixels === 'undefined' || edetailing.popupActual.dataPixels === false) {
            edetailing.popupActual.dataPixels = edetailing.popupDefault.pixels;
        }
        edetailing.popupActual.dataTemps = popup.attr('data-temps');
        if (typeof edetailing.popupActual.dataTemps === 'undefined' || edetailing.popupActual.dataTemps === false) {
            edetailing.popupActual.dataTemps = edetailing.popupDefault.temps;
        }

        switch (edetailing.popupActual.dataAnimacio) {
            case "slide-bottom":
                popup.animate({top: "+=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "slide-top":
                popup.animate({bottom: "+=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "fade":
                popup.css('z-index', 50);
                popup.animate({opacity: 1}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "slide-left":
                popup.animate({right: "+=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "slide-right":
                popup.animate({left: "+=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "none":
                popup.show();
                edetailing.popupActual.dataTemps = 0;
                break;
            default:
                popup.animate({top: "+=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
        }
        /** mini fix per al problema de alopez (popup s'obre i es tanca immediantament) **/
        setTimeout(function () {
            overlay.css('z-index', '49');
            overlay.animate({'opacity': edetailing.overlayDefault.opacity}, edetailing.popupActual.dataTemps);
        }, 100);

        /*** si cal guardar l'obertura de popups en un clickstream object **/
        if (edetailing.saveClickstreamPopup) {
            var identificadorPopup = popup.data('identificador');
            if (typeof identificadorPopup === 'undefined' || identificadorPopup === false) {
                // si no hi ha identificador, vol dir que no cal guardar-ho en aquest popup concret
                return;
            }
            var clickStream = {};
            clickStream.Popup_Opened_vod__c = true;
            clickStream.Track_Element_Id_vod__c = identificadorPopup;  //nomslide_popup_num * necessari
//            clickStream.Track_Element_Description_vod__c = "Popup opened at ...?"; // opcional
            edetailing.saveClickStreamData(clickStream);
        }
    },
    /**
     * 
     */
    closePopup: function () {
        var popup = $(edetailing.popupActual.idPopup);
        var overlay = $("#overlay");

        switch (edetailing.popupActual.dataAnimacio) {
            case "slide-bottom":
                popup.animate({top: "-=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "slide-top":
                popup.animate({bottom: "-=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "fade":
                popup.animate({opacity: 0}, edetailing.popupActual.dataTemps, function () {
                    popup.css('z-index', -199);
                });
                break;
            case "slide-left":
                popup.animate({right: "-=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "slide-right":
                popup.animate({left: "-=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
            case "none":
                popup.hide();
                break;
            default:
                popup.animate({top: "-=" + edetailing.popupActual.dataPixels}, edetailing.popupActual.dataTemps, function () {
                });
                break;
        }
        overlay.css('z-index', '-199');
        overlay.animate({'opacity': '0'}, edetailing.popupActual.dataTemps);

        edetailing.btnObridorPopup.removeClass(edetailing.classeBtnObridorPopupActiu);
        edetailing.btnObridorPopup = '';
    },
    
    /**
     * Add swipe events on the passed element, according to data parameters added in the element
     * @param {type} element
     * @returns {undefined}
     */
    hammerSwipe: function (element) {
        console.log('init - Hammer swipe');
        var mc = new Hammer(element);
        var aixo = $(element);
        var prev = aixo.data('prev');
        var next = aixo.data('next');

        if (prev) {
            mc.on("swiperight", function (ev) {
                if (prev === "goto") {
                    edetailing.crearEnllac(element, 'goto', 'prev');
                } else {
                    // Guardar slide anteriors i actual
                    sessionStorage.setItem("slideAnterior", edetailing.indexSlideActual);
                    sessionStorage.setItem("slideActual", edetailing.indexSlideActual - 1);
                    com.veeva.clm.prevSlide();
                }

            });
        }
        if (next) {
            mc.on("swipeleft", function (ev) {
                if (next === "goto") {
                    edetailing.crearEnllac(element, 'goto', 'next');
                } else {
                    // Guardar slide anteriors i actual
                    sessionStorage.setItem("slideAnterior", edetailing.indexSlideActual);
                    sessionStorage.setItem("slideActual", edetailing.indexSlideActual + 1);
                    com.veeva.clm.nextSlide();
                }
            });
        }
    },
    /**
     * Afegeix la classe "pressed" al element indicat
     * @param {type} e
     */
    startReact: function (e) {
        this.classList.add("pressed");
    },
    /**
     * Elimina la classe "pressed" al element indicat
     * @param {type} e
     */
    endReact: function (e) {
        this.classList.remove("pressed");
    },
    /**
     * Canvia un atribut de overlay dels popups
     * @param {type} attribut: opacity
     * @param {type} valor: decimal entre 0 i 1 per a la opacitat
     */
    changeOverlayDefault: function (attribut, valor) {
        edetailing.overlayDefault[attribut] = valor;
    },
    /**
     * Canvia el valor per defecte d'un atribut dels popups
     * @param {type} attribut: els atributs disponibles són: animacio, pixels, temps
     * @param {type} valor: els valors disponibles per a cada atribut són:
     *  - animacio: "slide-bottom", "slide-top","slide-left", "slide-right", "fade" i "none"
     *  - pixels: integer dels pixels que s'ha de desplaçar
     *  - temps: integer dels milisegons que ha de durar l'animació
     */
    changePopupDefault: function (attribut, valor) {
        edetailing.popupDefault[attribut] = valor;
    },
    /**
     * Els events touch per defecte són "touchend", però es pot modificar
     * @param {type} valor: "touchend", "touchstart"
     */
    changeTouchEventDefault: function (valor) {
        edetailing.defaultTouchEvent = valor;
    },
    /**
     * Modificar el nom de client per defecte
     * @param {type} valor: nom del client
     */
    changeDefaultAccountName: function (valor) {
        edetailing.defaultAccountName = valor;
    },
    /**
     * Crida a la funció de Veeva per obtenir el nom del Client i guarda el resultat
     * en session storage i el mostra en tots els elements amb la classe .account-name 
     * Si estem en mode debug no es fa la crida i es posa el nom de client per defecte.
     */
    getAccountName: function getAccountName() {
        if (!edetailing.testMode) {
            console.info('Getting account name');
            com.veeva.clm.getDataForCurrentObject("Account", "Name", function (result) {
                if (result.success) {
                    sessionStorage.setItem("accountName", result.Account.Name);

                } else {
                    sessionStorage.setItem("accountName", edetailing.defaultAccountName);
                }
                $('.account-name').text(sessionStorage.getItem("accountName"));
            });
        } else {
            sessionStorage.setItem("accountName", edetailing.defaultAccountName);
            $('.account-name').text(sessionStorage.getItem("accountName"));
        }


    },
    /**
     * Guarda un clickstream data amb la llibreria de Veeva.
     * @param {type} clickStream_data
     * @param {type} auxClick
     */
    saveClickStreamData: function saveClickStreamData(clickStream_data, auxClick) {
        var idClickstream = auxClick || 'clickstream_generic';
        com.veeva.clm.createRecord("Call_Clickstream_vod__c", clickStream_data, function (result) {
            if (result.success) {
                sessionStorage.setItem(idClickstream + '_desat', true);
            } else {
                sessionStorage.setItem(idClickstream + '_desat', false);
            }
        });
    },
    isIrepPresentation: function isIrepPresentation() {
        if (!edetailing.testMode) {
            var result = !com.veeva.clm.isEngage();
            sessionStorage.setItem("isIrepPresentation", result);
            if (result) {
                $('.only-irep').addClass('hidden');
                $('body').addClass('web-mode');
            }
            return result;

        } else {
            sessionStorage.setItem("isIrepPresentation", 'debug');
            return 'debug';
        }
    }

};

/**
 * Automenú
 * @param {type} estrMenu: estructura i elements del menú
 * @param {type} idContainer: id del element html on s'afegirà el menú
 */
function Menu(estrMenu, idContainer) {
    var aixo = this;
    aixo.idContainer = idContainer;
    aixo.estrMenu = estrMenu;
    aixo.idOverlayMenu = idContainer + '-overlay';

    aixo.nivellDesplegament = 0;

    aixo.init = function init(indexActius) {
        console.info('init - Automenu');
        //afegim active = true a l'estructura abans de muntar-la
        if (indexActius.nivell_1) {
            aixo.estrMenu[indexActius.nivell_1].active = true;
            if (indexActius.nivell_2) {
                aixo.estrMenu[indexActius.nivell_1].children[indexActius.nivell_2].active = true;
                if (indexActius.nivell_3) {
                    aixo.estrMenu[indexActius.nivell_1].children[indexActius.nivell_2].children[indexActius.nivell_3].active = true;
                    if (indexActius.nivell_4) {
                        aixo.estrMenu[indexActius.nivell_1].children[indexActius.nivell_2].children[indexActius.nivell_3].children[indexActius.nivell_4].active = true;
                    }
                }
            }
        }
        var menuContainer = $('#' + aixo.idContainer);
        var menuContent = aixo.montaMenu(aixo.estrMenu, 1);
        menuContainer.append(menuContent);
        $('#global').append('<div class="overlay menu-overlay" id="' + aixo.idOverlayMenu + '"></div>');

        var btnDesplegaSubmenu = document.getElementById(idContainer).getElementsByClassName('js-desplega-submenu');
        for (var i = 0; i < btnDesplegaSubmenu.length; i++) {
            btnDesplegaSubmenu[i].addEventListener('touchend', aixo.toggleSubmenu);
        }
        var overlay = document.getElementById(aixo.idOverlayMenu);
        overlay.addEventListener('touchend', aixo.hideSubmenus);

    };
    aixo.montaMenu = function montaMenu(estrMenu, nivell) {
        var menuContent = '<ul class="nivell-' + nivell + '">';
        for (var i = 0; i < estrMenu.length; i++) {
            menuContent += aixo.montaElement(estrMenu[i], nivell);
        }
        menuContent += '</ul>';
        return menuContent;
    };
    aixo.montaElement = function montaElement(element, nivell) {
        var text = (element.text) ? element.text : '';
        var active = (element.active) ? ' active' : '';
        var classes = (element.classes) ? ' ' + element.classes : '';
        var ic = (element.ic) ? '<span class="ic ' + element.ic + '"></span>' : '';
        var goto = '';
        var slide = '';
        var presentation = '';
        var camins = '';
        if (element.goto) {
            goto = (element.goto.touch) ? 'js-goto-touch ' : 'js-goto ';
            slide = (element.goto.slide) ? ' data-slide="' + element.goto.slide + '" ' : '';
            presentation = (element.goto.presentation) ? ' data-presentation="' + element.goto.presentation + '" ' : '';
            if (element.goto.camins) {
                var arrayCamins = element.goto.camins;
                for (var i = 0; i < arrayCamins.length; i++) {
                    camins += (arrayCamins[i].presentation) ? ' data-presentation-' + arrayCamins[i].num + '="' + arrayCamins[i].presentation + '" ' : '';
                    camins += (arrayCamins[i].slide) ? ' data-slide-' + arrayCamins[i].num + '="' + arrayCamins[i].slide + '" ' : '';
                }
            }
        }

        var popup = '';
        var dataPopup = '';
        if (element.popup) {
            popup = ' js-mostrar-popup';
            dataPopup = 'data-popup="' + element.popup + '"';
        }

        var submenu = (element.children) ? aixo.montaMenu(element.children, nivell + 1) : '';
        var desplegaSubmenu = (element.children) ? ' js-desplega-submenu' : '';

        return '<li class="' + popup + goto + active + classes + '"' + dataPopup + slide + presentation + camins + '><span class="' + desplegaSubmenu + '">' + ic + text + '</span>' + submenu + '</li>';
    };
    aixo.toggleSubmenu = function toggleSubmenu(e) {
        e.stopPropagation();
        var parent = $(this).parent('li');

        parent.siblings('li').children('ul').hide();
        parent.siblings('li').children('ul').children('li').children('ul').hide();

        parent.siblings('li').removeClass('focused');
        parent.siblings('li').children('ul').children('li').removeClass('focused');
        parent.siblings('li').children('ul').children('li').children('ul').children('li').removeClass('focused');

        if (parent.children('ul').is(':visible')) {
            aixo.nivellDesplegament--;
            parent.removeClass('focused').children('ul').hide();
        } else {
            aixo.nivellDesplegament++;
            parent.addClass('focused').children('ul').show();
        }
        if (aixo.nivellDesplegament === 0) {
            $('#' + aixo.idOverlayMenu).hide();
        } else {
            $('#' + aixo.idOverlayMenu).show();
        }
    };

    aixo.hideSubmenus = function hideSubmenus(e) {
        aixo.nivellDesplegament = 0;
        $('#' + aixo.idContainer + ' .nivell-2, ' + '#' + aixo.idContainer + ' .nivell-3, ' + '#' + aixo.idOverlayMenu).hide();
        $('#' + aixo.idContainer + ' .focused').removeClass('focused');
    };


    aixo.disableMenuItem = function disableMenuItem(index, menuLevel) {
        //
        index++;
        $('.nivell-' + menuLevel + ' li:nth-child(' + index + ')').addClass('disabled');
    };


}

/**
 * 
 * @param {type} estrMenu: array dels elements per a muntar els bullets, pot ser un element de la variable "estructura", de la estructuraMenu o un array simple
 * @param {type} idContainer: id del element html on s'afegiran els Bullets
 * @param {type} mode: hi ha 3 modes:
 *     0 --> els bullets es muntent sense enllaç
 *     1 --> els bullets es munten amb enllaç intern, només utilitzant data-slide (estrMenu parteix de la var "estructura" per exemple estructura[0][1])
 *     2 --> els bullets es munten amb l'enllaç que apareix en l'element (estrMenu parteix de la var "estructuraMenu" i per tant agafa les dades per a l'enllaç del goto
 */
function Bullets(estrMenu, idContainer, mode) {
    var aixo = this;
    aixo.idContainer = idContainer;
    aixo.estrMenu = estrMenu;
    aixo.mode = mode;
    aixo.slice = false;
    aixo.dataSlice;

    /**
     * 
     * @param {type} indexActual
     * @returns {undefined}
     */
    aixo.init = function init(indexActual) {
        console.info('init Bullets');
        var bullets = aixo.montaBoletes(indexActual);
        $('#' + idContainer).append(bullets);
    };

    aixo.sliceEstrMenu = function sliceEstrMenu(indexInicial, indexFinal) {
        aixo.slice = true;
        aixo.dataSlice = {"indexInicial": indexInicial, "indexFinal": indexFinal};
    };

    /**
     * 
     * @param {type} indexActual
     * @returns {Bullets.montaBoletes.boletesContent}
     */
    aixo.montaBoletes = function montaBoletes(indexActual) {
        var boletesContent = '';
        var i, max;
        if (aixo.slice) {
            i = aixo.dataSlice.indexInicial;
            max = aixo.dataSlice.indexFinal;
            indexActual = indexActual + i;
        } else {
            i = 0;
            max = aixo.estrMenu.length;
        }
        for (i; i < max; i++) {
            if (i === indexActual) {
                boletesContent += '<span class="bullet active"></span>';
            } else {
                switch (aixo.mode) {
                    case 1:
                        boletesContent += '<span class="js-goto-touch bullet" data-slide="' + i + '"></span>';
                        break;
                    case 2:
                        var slide = (aixo.estrMenu[i].goto.slide) ? 'data-slide="' + aixo.estrMenu[i].goto.slide + '"' : '';
                        var presentation = (aixo.estrMenu[i].goto.presentation) ? 'data-presentation="' + aixo.estrMenu[i].goto.presentation + '"' : '';
                        boletesContent += '<span class="js-goto-touch bullet" ' + slide + ' ' + presentation + '></span>';
                        break;
                    default:
                        boletesContent += '<span class="bullet"></span>';
                        break;
                }
            }
        }
        return boletesContent;
    };
}

