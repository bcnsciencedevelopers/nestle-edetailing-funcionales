/* Author: Ruth CJ */
var resposta;
jQuery(document).ready(function () {

    var indexActius = {"nivell_1": "0", "nivell_2": false, "nivell_3": false};
    menuPrincipal.init(indexActius);

    //Inicialitzar l'edetailing 
    edetailing.init(estructura);


    // CLICKSTREAM Get data form sessionStorage and prepare data for the slider
    resposta = sessionStorage.getItem('resposta1');
    calculateSlide(parseInt(resposta));
    var respostaAnterior = resposta || 0;
   
    $('.js-save-clickstream').on('touchstart', function (e) {
        console.log('save');
        auxPregunta = "pregunta1";
        var aixo = $(this);
        if (resposta !== null) {
            var desat = sessionStorage.getItem(auxPregunta + 'desat');
            sessionStorage.setItem('resposta1', resposta);
            if (typeof desat === 'undefined' || desat === null || desat === "false") {
                var clickStream = {};
                clickStream.Answer_vod__c = resposta + "%";
                clickStream.Question_vod__c = "¿Consideras relevante el L. reuteri en las fórmulas funcionales?";
                clickStream.Survey_Type_vod__c = "choose one";
                clickStream.Possible_Answers_vod__c = "0%, 25%, 50%, 75%, 100%";
                clickStream.Track_Element_Description_vod__c = "ES_2017_IF_edetailing_funcionales__Consideras_relevante_Lreuteri_en_formulas_funcionales";
                saveClickStreamData(clickStream);
            }
        }
    });


    $("#slider").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 100,
        step: 25,
        value: respostaAnterior,
        slide: function (event, ui) {
            var result = calculateSlide(ui.value);
            resposta = ui.value;
        }
    });



});

function calculateSlide(value) {
    console.log(value);
    var urlImage;
    var slideToGo;
    switch (value) {
        case 0:
            slideToGo = 0;
            urlImage = "url('css/images/grafica_02.png')";
            break;
        case 25:
            slideToGo = 0;
            urlImage = "url('css/images/grafica_02.png')";
            break;
        case 50:
            slideToGo = 0;
            urlImage = "url('css/images/grafica_03.png')";
            break;
        case 75:
            slideToGo = 5;
            urlImage = "url('css/images/grafica_04.png')";
            break;
        case 100:
            slideToGo = 5;
            urlImage = "url('css/images/grafica_05.png')";
            break;
        default:
            slideToGo = 0;
            urlImage = "url('css/images/grafica_01.png')";
            break;
    }
    $("#boton-goto").attr('data-slide', slideToGo).css('background-image', urlImage);
    
    return {urlImage: urlImage, slideToGo: slideToGo};
    
}

